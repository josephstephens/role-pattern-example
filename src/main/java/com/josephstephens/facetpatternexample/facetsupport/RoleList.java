package com.josephstephens.facetpatternexample.facetsupport;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RoleList<T>
{
  private final Map<Class<? extends Role<T>>, Role<T>> facets = new HashMap<>();

  private final T owner;

  public RoleList(T owner) {
    this.owner = owner;
  }

  public <U extends Role<T>> Optional<U> get(Class<U> type) {
    return Optional.ofNullable((U) facets.get(type));
  }

  public void add(Role<T> role) {
    facets.put((Class<? extends Role<T>>) role.getClass(), role);
    role.attach(owner);
  }
}
