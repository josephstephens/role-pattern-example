package com.josephstephens.facetpatternexample.facetsupport;

public abstract class Role<T>
{

  private T owner;

  void attach(T t) {
    this.owner = t;
  }

  protected T getOwner() {
    return owner;
  }
}
