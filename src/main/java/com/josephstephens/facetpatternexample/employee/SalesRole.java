package com.josephstephens.facetpatternexample.employee;

import com.josephstephens.facetpatternexample.facetsupport.Role;

public class SalesRole
    extends Role<Employee>
{

  public String[] getSalesContacts() {
    return new String[]{"joe", "bob", "john"};
  }
}
