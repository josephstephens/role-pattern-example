package com.josephstephens.facetpatternexample.employee;

import com.josephstephens.facetpatternexample.facetsupport.Role;

public class PartTimeRole
    extends Role<Employee>
{

  public void generateSalaryReport() {
    System.out.println("The part time salary of " + getOwner().getName() + " is " + getOwner().getBaseSalary());
  }

}
