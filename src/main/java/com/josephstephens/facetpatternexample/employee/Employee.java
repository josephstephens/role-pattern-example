package com.josephstephens.facetpatternexample.employee;

import java.util.Optional;

import com.josephstephens.facetpatternexample.facetsupport.Role;
import com.josephstephens.facetpatternexample.facetsupport.RoleList;

public class Employee
{
  private final RoleList roleList = new RoleList(this);

  private double baseSalary;

  private String name;

  public Employee(Role<Employee>... roles) {
    for (Role role : roles) {
      roleList.add(role);
    }
  }

  public <T extends Role<Employee>> Optional<T> facet(Class<T> type) {
    return roleList.get(type);
  }

  public double getBaseSalary() {
    return baseSalary;
  }

  public void setBaseSalary(double baseSalary) {
    this.baseSalary = baseSalary;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
