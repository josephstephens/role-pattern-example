package com.josephstephens.facetpatternexample.employee;

import com.josephstephens.facetpatternexample.facetsupport.Role;

public class FullTimeRole
    extends Role<Employee>
{

  public void generateSalaryReport() {
    System.out.println("The full time salary of " + getOwner().getName() + " is " + getOwner().getBaseSalary());
  }
}
