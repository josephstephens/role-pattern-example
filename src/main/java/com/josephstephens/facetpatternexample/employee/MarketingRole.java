package com.josephstephens.facetpatternexample.employee;

import com.josephstephens.facetpatternexample.facetsupport.Role;

public class MarketingRole
    extends Role<Employee>
{

  private boolean websiteAccess;

  public boolean isAllowedToModifyWebsite() {
    return websiteAccess;
  }
}
