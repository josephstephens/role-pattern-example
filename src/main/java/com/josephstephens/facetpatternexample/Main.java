package com.josephstephens.facetpatternexample;

import java.util.Optional;

import com.josephstephens.facetpatternexample.employee.Employee;
import com.josephstephens.facetpatternexample.employee.FullTimeRole;
import com.josephstephens.facetpatternexample.employee.SalesRole;

public class Main
{

  public static void main(String[] args) {
    Employee fullTimeSalesEmployee = new Employee(new FullTimeRole(), new SalesRole());

    Optional<FullTimeRole> fullTime = fullTimeSalesEmployee.facet(FullTimeRole.class);
    if (fullTime.isPresent()) {

    }
  }
}
